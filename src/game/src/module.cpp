#include <game/module.hpp>
#include <game/updater.hpp>
#include <game/presenter.hpp>
#include <game/index.hpp>
#include <com/frame.hpp>

namespace gam {

void boot(com::Folder* const ctx_root)
{
    com::Folder* const root_mak = ctx_root->push_back<com::Folder>("game");
    root_mak->push_back<com::Folder>("camera")->push_back<com::Frame>();
    root_mak->push_back<com::Folder>("grid")->push_back<com::Frame>();
    root_mak->push_back<Updater>(); 
    root_mak->push_back<Presenter>(); 
    gam::index();
}

void shutdown(com::Folder* const ctx_root)
{
    com::Folder* const root_mak = ctx_root->find<com::Folder>("game");
    root_mak->erase(root_mak->find<Presenter>(Presenter::self_name()));
    root_mak->erase(root_mak->find<Updater>(Updater::self_name()));
    root_mak->erase(root_mak->find<com::Folder>("grid"));
    root_mak->erase(root_mak->find<com::Folder>("camera"));
    ctx_root->erase(root_mak);
}

}
