#ifndef GAME_PROGRAM_INFO_HPP_INCLUDED
#   define GAME_PROGRAM_INFO_HPP_INCLUDED

#   include <string>

namespace gam {

std::string  get_program_name();
std::string  get_program_version();
std::string  get_program_description();

}

#endif
