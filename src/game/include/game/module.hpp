#ifndef GAME_MODULE_HPP_INCLUDED
#   define GAME_MODULE_HPP_INCLUDED

namespace com { struct Folder; }

namespace gam {

void boot(com::Folder* ctx_root);
void shutdown(com::Folder* ctx_root);

}

#endif
