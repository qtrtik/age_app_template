# **Game**

The package contains a template for construction of an application (game)
with a support of the **age** engine. The engine comes in a form of C++
libraries **libraries**. One can optionally also include **data**
and the **maker** tool (scene editor) to the constructed project.

All mentioned components of **age** should be included to the project as
submodules. This process is automated using the script `bootstrap.py`.
Comment out components which you do not want to include into your project.

The second step in the customization is in the root `CMakeLists.txt` file,
where you should change the name of the project.

The last customization step is in the file `./src/CMakeLists.txt`, where
you can comment out **age** components (libraries and the tool), which
you do not want to include into building of your project. That is done
via variables `AGE_LIBRARIES_TO_USE` and `AGE_TOOLS_TO_USE`.

The rest of this document is dedicated to building **age** components
which can be included into your project.

IMPORTANT: Do not clone this repository. Create your own new repository
into which you copy the content of this repository (i.e., just download
sources as ZIP file and unpack it to your repository).

## **age** basic info

The name **age** is an acronym of '**a**cademic **g**ame **e**ngine'. It is
a codebase and playground for students of the game development study
program. It is primarily used for seminar tasks and thesis.

## License

**age** is available under the **zlib** license. It is included in the package
as the file `LICENSE.txt`.

## Target platforms

The primary target platform is PC running either Windows 10 (or later) or Ubuntu
22.04 (or later) operating systems.

## Software dependencies

The following software must be installed on your computer before you can
start with the **age** project:
- **git** distributed version control system: https://git-scm.com/
    - (optional) Configure your git in a console using these commands: 
        ```
        git config --global user.name "Your name"
        git config --global user.email "Your email"
        git config --global core.autocrlf false
        git config --global core.filemode false
        git config --global color.ui true
        ```
- **C++ compiler** supporting at least **C++17** standard:
    - On Ubuntu use one of these two options:
        - **g++** of GCC: https://en.wikipedia.org/wiki/GNU_Compiler_Collection
        - **Clang**: https://clang.llvm.org/ and https://en.wikipedia.org/wiki/Clang
        - NOTE: Consider using this command for installing the compiler:
            ```
            sudo apt install gcc g++
            ```
    - On Windows use the **Microsoft C++** compiler and debugger:
        1. Go to page: https://visualstudio.microsoft.com/downloads/#other
        2. Search for **Tools for Visual Studio 2022** and click on the text to open nested items.
        3. Search for **Build Tools for Visual Studio 2022** nested item and click on the
           **Download** button.
- **CMake** build system: https://cmake.org/
  - NOTE: On Ubuntu consider using this command:
    ```
    sudo apt install make cmake ninja-build
    ```
- **vcpkg** software package manager: https://github.com/microsoft/vcpkg
  - NOTE: On Ubuntu we recommend to use this command first:
    ```
    sudo apt install curl pkg-config libgl1-mesa-dev libegl1-mesa-dev
    sudo apt install libx11-dev libxft-dev libxext-dev libwayland-dev libxkbcommon-dev libegl1-mesa-dev libibus-1.0-dev
    sudo apt install pkg-config autoconf-archive
    ```
  - Once you have the package manager installed, install into it required packages:\
    `vcpkg install sdl2 glad glm boost eigen3 stb gtest --clean-after-build`\
    On Windows append the option `--triplet=x64-windows` to the command and `--triplet=x64-linux` on Ubuntu.
- **Microsoft Visual Studio Code** (VS code) source-code editor: https://code.visualstudio.com/
    - Once you have the editor installed, install into it these extensions:
        - **C/C++** by Microsoft: https://github.com/microsoft/vscode-cpptools
        - **C/C++ Extension Pack** by Microsoft: https://github.com/microsoft/vscode-cpptools
        - **C/C++ Themes** by Microsoft: https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools-themes
        - **CMake Tools** by Microsoft: https://github.com/microsoft/vscode-cmake-tools
        - (optional) **CMake** by twxs: https://github.com/twxs/vs.language.cmake
        - (optional) **Git Graph** by mhutchie: https://github.com/mhutchie/vscode-git-graph
        - (optional) **Code Spell Checker** by Street Side Software: https://github.com/streetsidesoftware/vscode-spell-checker
    - It is common and useful to use `launch.json` config file for launching an
        executable. That way you can specify command-line parameters for the
        executable. The initial (minimal) version is under `setup` folder. You
        only need to copy the file from the `setup` folder to the folder
        `.vscode` (create this folder, if it does not exist). 
- (optional) **SmartGit** Git GUI client: https://www.syntevo.com/smartgit/

## Integrating **vcpkg**

### Visual Studio Code
Before we can build **age** in VS Code, we must let VS Code to know
where is **vcpkg** installed (because it contains SW packages **age**
needs during the build process). We must create file

```
<project-root-dir>/.vscode/settings.json
```

with this content:

```
{
    "cmake.configureSettings": {
        "CMAKE_TOOLCHAIN_FILE": "<vcpkg-install-dir>/scripts/buildsystems/vcpkg.cmake",
        "CMAKE_BUILD_TYPE": "${buildType}"
    }
}
```
where `<vcpkg-install-dir>` must be replaced by the actuall installation directory of **vcpkg**.

NOTE: When working on Windows, VS Code may have created a "global" 
settings file here:
```
<user-dir>/AppData/Roaming/Code/User/settings.json
```
Instead of creating the new settings file as described above, you
can just update this existing "global" setting file by adding the section:
```
    "cmake.configureSettings": {
        "CMAKE_TOOLCHAIN_FILE": "<vcpkg-install-dir>/scripts/buildsystems/vcpkg.cmake",
        "CMAKE_BUILD_TYPE": "${buildType}"
    }
```
The advantage of this approach is, that the **vcpkg** integration
to VS Code would work for all CMake C++ projects on your computer.

### CLion
Clion can work with an existing **vcpkg** installation, or it can install it automatically.
To integrate **vcpkg**, follow the instructions in the official CLion Blog: [Support for vcpkg in CLion](https://blog.jetbrains.com/clion/2023/01/support-for-vcpkg-in-clion/)

## Building **age**
### Visual Studio Code
Open **Microsoft Visual Studio Code** and in the main menu choose:
`File/Open Folder...` and open the directory `<project-root-dir>`.

Now you should be able to build the project the same way as any other
CMake C++ application. All needed information are available here:
https://code.visualstudio.com/docs/cpp/introvideos-cpp

Once you successfully build the `install` target, then you can find
the built game under the `dist` directory.

**age** looks for data according to the `cwd`, please run **age** from the tasks
provided or directly from any folder containing its data. This means, running the 
project from folder `<project-root-dir>/build` via `../dist/maker` will not work as 
opposed to running it directly from `<project-root-dir>/dist`.

### CLion
Open the root directory via _File > Open_ and build the `install` target with _Build > install_.
If you need help with building and running a CMake C++ application, take a look at the CLion [Quick CMake tutorial](https://www.jetbrains.com/help/clion/quick-cmake-tutorial.html).

CLion should automatically load all the CMake targets, however, you will need to configure
the `maker` target to run it correctly. To do that, click on the CMake targets dropdown menu
(on the right side of the upper bar, next to the _build_ icon) and select _Edit Configurations..._.
Then click on the `maker` target and set the working directory to `<project-root-dir>/dist`.

## Understanding the source code

**age** is basically a collection of libraries operating over a single
data structure, which can be seen an in-memory disk. This disk itself is
also a library. 

For each library, there is included a `README.md` file where is described
the purpose and functionality of the library. Details about usage
and implementation of the library is then available via source code comments.
