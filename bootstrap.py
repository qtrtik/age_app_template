#!/usr/bin/env python3
import subprocess

subprocess.run(["git", "init"])
subprocess.run(["git", "submodule", "add", "https://gitlab.fi.muni.cz/gamedev/age/libraries.git", "./src/age"])
subprocess.run(["git", "submodule", "add", "https://gitlab.fi.muni.cz/gamedev/age/data.git", "./data/age"])
# subprocess.run(["git", "submodule", "add", "https://gitlab.fi.muni.cz/gamedev/age/maker.git", "./src/maker"])
